
public class Konsolenausgabe2 {
	
	
	

	public static void main(String[] args) {

		System.out.println("Konsolenausgabe2");
		int alter = 22; 
		String name = "Shkelqim"; 
		System.out.print("Ich hei�e " + name + " und bin " + alter + " Jahre alt.\nDas ist meine erste richtige Programmierung. Und es gef�llt mir:)\n");
		
		// Der Unterschied zwischen print und println besteht darin, dass println einen Zeilenumbruch nach der Ausgabe einf�gt.
		// Das sieht man an dem Beispiel 
		
		System.out.println("Hallo, wie geht es dir?");
		System.out.println("Mir geht es gut, danke\n");
		
		System.out.print("Auch gut, danke. ");
		System.out.print("Was ist dein Plan f�r heute?\n \n \n");
		
		//Hier sieht man, dass bei println ein Zeilenumbruch entsteht und bei print nicht, es sei denn, man f�ght /n hinzu
		
		
		System.out.printf( "%8s\n", "*" );
		System.out.printf( "%9s\n", "***" );
		System.out.printf( "%10s\n", "*****" );
		System.out.printf( "%11s\n", "*******" );
		System.out.printf( "%12s\n", "*********" );
		System.out.printf( "%13s\n", "***********" );
		System.out.printf( "%14s\n", "*************" );
		System.out.printf( "%15s\n", "***************" );
		System.out.printf( "%9s\n", "***" );
		System.out.printf( "%9s\n \n \n \n", "***" );
		
		System.out.printf("%.2f \n", 22.42);
		System.out.printf("%.2f \n", 111.22);
		System.out.printf("%.2f \n", 4.0);
		System.out.printf("%.2f \n", 1000000.55);
		System.out.printf("%.2f \n \n", 97.34);
		
		
		
		System.out.println("A1.6 Aufgabe 1 \n \n");
		
		System.out.printf( "%11s\n", "** \n" );
		System.out.printf( "%3s  ", "*" ); 
		System.out.printf( "%9s \n  ", "*" );
		System.out.printf( "%-11s", "*" );
		System.out.printf( "%-5s \n", "* \n" );
		System.out.printf( "%9s\n\n", "**" );
		
		System.out.println("A1.6 Aufgabe 2 \n \n");
		
		System.out.printf("%-5s = ", "0!");
		System.out.printf("%-19s = ", "");
		System.out.printf("%4s\n", "1");
		
		System.out.printf("%-5s = ", "1!");
		System.out.printf("%-19s = ", "1");
		System.out.printf("%4s\n", "1");
		
		System.out.printf("%-5s = ", "2!");
		System.out.printf("%-19s = ", "1 * 2");
		System.out.printf("%4s\n", "2");
		
		System.out.printf("%-5s = ", "3!");
		System.out.printf("%-19s = ", "1 * 2 * 3");
		System.out.printf("%4s\n", "6");
		
		System.out.printf("%-5s = ", "4!");
		System.out.printf("%-19s = ", "1 * 2 * 3 * 4");
		System.out.printf("%4s\n", "24");
		
		System.out.printf("%-5s = ", "5!");
		System.out.printf("%-19s = ", "1 * 2 * 3 * 4 * 5");
		System.out.printf("%4s\n \n", "120");
		
		System.out.println("A1.6 Aufgabe 3 \n \n");
		
		
		System.out.printf("%-12s|", "Fahrenheit");
		System.out.printf("%10s\n", "Celsius");
		System.out.println("-----------------------");
		
		System.out.printf("%-12s|", "-20");
		System.out.printf("%10.2f\n", -28.8889);
		
		System.out.printf("%-12s|", "-10");
		System.out.printf("%10.2f\n", -23.3333);
		
		System.out.printf("%-12s|", "0");
		System.out.printf("%10.2f\n", -17.7778);
		
		System.out.printf("%-12s|", "+20");
		System.out.printf("%10.2f\n", -6.67);
		
		System.out.printf("%-12s|", "+30");
		System.out.printf("%10.2f\n", -1.1111);
		

	}

}
